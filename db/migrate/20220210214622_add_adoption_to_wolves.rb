class AddAdoptionToWolves < ActiveRecord::Migration[7.0]
  def change
    add_column :wolves, :adopter_name, :string
    add_column :wolves, :adopter_email, :string
    add_column :wolves, :adopter_age, :integer
    add_column :wolves, :adopted, :boolean, default: false
  end
end
