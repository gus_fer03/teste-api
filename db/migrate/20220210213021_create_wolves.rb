class CreateWolves < ActiveRecord::Migration[7.0]
  def change
    create_table :wolves do |t|
      t.string :name
      t.string :description
      t.string :image_url
      t.integer :age

      t.timestamps
    end
  end
end
