require "test_helper"

class WolvesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @wolf = wolves(:one)
  end

  test "should get index" do
    get wolves_url, as: :json
    assert_response :success
  end

  test "should create wolf" do
    assert_difference("Wolf.count") do
      post wolves_url, params: { wolf: { age: @wolf.age, description: @wolf.description, image_url: @wolf.image_url, name: @wolf.name } }, as: :json
    end

    assert_response :created
  end

  test "should show wolf" do
    get wolf_url(@wolf), as: :json
    assert_response :success
  end

  test "should update wolf" do
    patch wolf_url(@wolf), params: { wolf: { age: @wolf.age, description: @wolf.description, image_url: @wolf.image_url, name: @wolf.name } }, as: :json
    assert_response :success
  end

  test "should destroy wolf" do
    assert_difference("Wolf.count", -1) do
      delete wolf_url(@wolf), as: :json
    end

    assert_response :no_content
  end
end
