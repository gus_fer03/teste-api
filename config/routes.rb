Rails.application.routes.draw do
  get 'wolves/adopted'
  resources :wolves
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
end
