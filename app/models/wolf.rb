class Wolf < ApplicationRecord
    validates :name, :description, :image_url, :age, presence: true
    validates :description, length: { minimum: 5, maximum: 150 }
    validates :age, numericality: { greater_than: 0 }
end
